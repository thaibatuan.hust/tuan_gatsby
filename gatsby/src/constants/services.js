import React from "react"
import { FaCode, FaSketch, FaAndroid } from "react-icons/fa"
export default [
  {
    id: 1,
    icon: <FaCode className="service-icon" />,
    title: "Data Scientist",
    text: `I have master 2 degree Artificial Intelligence, Systems, Data at Paris Dauphine, I can cleaning, pre-processing data and create, build, deploy Machine Learning, Deep Learning mode`,
  },
  {
    id: 2,
    icon: <FaSketch className="service-icon" />,
    title: "Software development",
    text: `I have a degree in Information System at Hanoi university of science and technology and 1 year experience in build, deploy iOS application and website`,
  },
  {
    id: 3,
    icon: <FaAndroid className="service-icon" />,
    title: "DevOps",
    text: `Currently, I'm on the way to learning DepOps to become MLOps engineer`,
  },
]
