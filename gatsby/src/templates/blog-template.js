import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/Layout"
import ReactMarkdown from "react-markdown"
import SEO from "../components/SEO"
import ReactDOM from 'react-dom'
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco, githubGist, darcula, vs, tomorrowNightEighties } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import CodeCopyBtn from "./codeCopyBtn"

const ComponentName = ({ data }) => {
  const { content, title, desc } = data.blog
  const Pre = ({ children }) => <pre className="blog-pre">
        <CodeCopyBtn>{children}</CodeCopyBtn>
        {children}
    </pre>
  return (
    <Layout>
      <SEO title={title} description={desc} />
      <section className="blog-template">
        <div className="section-center">
          <article className="blog-content">
            <ReactMarkdown 
            source={content} 
            className="markdown"
            escapeHtml={false}
            renderers={{
              pre: Pre,
              code: Component
            }}
            />
          </article>
          <Link to="/blog" className="btn center-btn">
            blog
          </Link>
        </div>
      </section>
    </Layout>
  )
}


const Component = ({value, language}) => {

  return (
    <SyntaxHighlighter language={language ?? null} style={githubGist}>
      {value ?? ''}
    </SyntaxHighlighter>
  );
};



export const query = graphql`
  query GetSingleBlog($slug: String) {
    blog: strapiBlogs(slug: { eq: $slug }) {
      content
      title
      desc
    }
  }
`

export default ComponentName
